import torch
import torch.nn as nn
import torch.nn.functional as F
import torch_geometric.data as geom_data
from torch_geometric.nn import GCNConv
from torch_geometric.utils import add_self_loops, degree
from torch_geometric.data import Data
from torch_geometric.loader import DataLoader

import numpy as np
from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import train_test_split


def parse_rtp(rtp: str) -> (dict, dict, dict, dict, dict):
	"""
	Reads an .rtp file to extract molecule definitions, separating them into
	dictionaries for: types, charges, impropers, dihedrals, bondedtypes
	:param rtp: str, path to the .rtp file
	:param remember: bool, whether to reuse .rtps selected before
	:return: tuple of dict, each containing atom name : relevant parameter matching
	(ordered: types, charges, dihedrals, impropers, bondedtypes)
	"""
	chargedict = {}
	bonds = {}
	rtp_cont = [line for line in open(rtp) if not line.strip().startswith(';')]
	resname = None
	reading_atoms = False
	reading_bonds = False
	forb = ['ALF4', 'BICB', 'TBRE']  # these contain uncommon elements, e.g. aluminium
	for line in rtp_cont:
		if line.strip().startswith('[') and line.strip().split()[1] not in ['bonds', 'atoms', 'bonds',
																			'dihedrals', 'impropers', 'bondedtypes']:
			resname = line.strip().split()[1]
			if resname not in chargedict.keys():
				chargedict[resname] = []
				bonds[resname] = []
		if line.strip().startswith('[') and line.strip().split()[1] == 'atoms':
			reading_atoms = True
		if line.strip().startswith('[') and line.strip().split()[1] != 'atoms':
			reading_atoms = False
		if line.strip().startswith('[') and line.strip().split()[1] == 'bonds':
			reading_bonds = True
		if line.strip().startswith('[') and line.strip().split()[1] != 'bonds':
			reading_bonds = False
		if len(line.strip().split()) > 3 and resname is not None and reading_atoms:
			chargedict[resname].append((line.strip().split()[0], float(line.strip().split()[2])))
		if len(line.strip().split()) == 2 and reading_bonds:
			bonds[resname].append(tuple(line.strip().split()))
	return {k: v for k, v in chargedict.items() if k not in forb}, {k: v for k, v in bonds.items() if k not in forb}

def preprocess_molecule_data(mol_bonds, mol_charges):
    graphs = []
    one_hot_encoder = OneHotEncoder()
    one_hot_encoder.fit(np.array(list('FCHONSP')).reshape(-1, 1))
    for key in mol_bonds.keys():
        m_b = mol_bonds[key]
        m_c = mol_charges[key]
        index_mapping = {name[0]: i for i, name in enumerate(m_c)}
        # Compute adjacency matrix
        n_atoms = len(m_c)
        adjacency_matrix = np.zeros((n_atoms, n_atoms))
        for bond in m_b:
            atom1, atom2 = bond
            adjacency_matrix[index_mapping[atom1], index_mapping[atom2]] = 1
            adjacency_matrix[index_mapping[atom2], index_mapping[atom1]] = 1
        
        # Create node feature matrix using one-hot encoding
        atom_names = [pair[0][0] for pair in m_c]
        try:
	        node_features = one_hot_encoder.transform(np.array(atom_names).reshape(-1, 1)).toarray()
        except:
            continue
        
        charges = np.array([pair[1] for pair in m_c]).reshape(-1, 1)  # Create target vector for charges
        adjacency = torch.tensor(adjacency_matrix, dtype=torch.float)
        features = torch.tensor(node_features, dtype=torch.float)
        charges = torch.tensor(charges, dtype=torch.float)
        edge_index = adjacency.nonzero()
        graphs.append(Data(x=features, edge_index=edge_index.T, y=charges))  # Create a Graph object and append it to the list
    
    return graphs

class MoleculeDataset(geom_data.Dataset):
    """
    A container that simply implements a list of graphs and
    allows for convenient processing of batches/splitting
    """
    def __init__(self, bond_data, charge_data, transform=None, pre_transform=None, **kwargs):
        self.graphs = preprocess_molecule_data(bond_data, charge_data)
        super().__init__(None, transform, pre_transform, **kwargs)
    
    def len(self):
        return len(self.graphs)
    
    def get(self, idx):
        return self.graphs[idx]

class ZeroSumOutputLayer(nn.Module):
    """
    Makes sure all charges sum up to 0 (not sure if all molecules in the dataset
    are actually neutral, can be improved)
    """
    def forward(self, x):
        mean = x.mean(dim=0, keepdim=True)
        return x - mean

# Define your GCN model
class GCNModel(nn.Module):
    """
    Performs the graph convolutions
    """
    def __init__(self, num_classes):
        super().__init__()
        self.graph_conv1 = GCNConv(-1, hidden_l)
        self.graph_conv2 = GCNConv(hidden_l, hidden_l)
        self.fc = nn.Linear(hidden_l, num_classes)
        self.zs = ZeroSumOutputLayer()
    
    def forward(self, data):
        x, edge_index, batch = data.x, data.edge_index, data.batch
        x = F.relu(self.graph_conv1(data.x, data.edge_index))
        x = F.relu(self.graph_conv2(x, data.edge_index))
        x = self.fc(x)
        x = self.zs(x)
        return x

chs, bs = parse_rtp('cgenff.rtp')

# Load your custom dataset
num_classes = 1  # Partial charges

hidden_l = 64
batch_size = 1
num_epochs = 1000
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

dataset = MoleculeDataset(bs, chs)
model = GCNModel(num_classes)
model.to(device)

train_dataset, val_dataset = train_test_split(dataset, test_size=0.2, random_state=42)

train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
val_loader = DataLoader(val_dataset, batch_size=batch_size, shuffle=False)

# Define your loss function
loss_func = nn.MSELoss()

# Define your optimizer
optimizer = torch.optim.Adam(model.parameters(), 0.001)

# Training loop
for epoch in range(num_epochs):
    model.train()  # indicates we're in training mode
    train_loss = 0.0
    for batch in train_loader:
        batch = batch.to(device)  # move data to GPU (where the model is already)
        optimizer.zero_grad()  # clears gradient for next batch optimization
        output = model(batch)  # Forward pass
        loss = loss_func(output, batch.y)  # Compute the loss
        loss.backward()  # propagates the loss through the comp graph
        optimizer.step()  # adjusts model parameters
        train_loss += loss.item() * batch.num_graphs
    
    # Compute average training loss for the epoch
    train_loss /= len(train_dataset)

    # Evaluation
    model.eval()  # indicates we're no longer in training mode
    val_loss = 0.0
    with torch.no_grad():
        for batch in val_loader:
            batch = batch.to(device)
            output = model(batch) # Forward pass
            loss = loss_func(output, batch.y) # Compute the loss
            val_loss += loss.item() * batch.num_graphs
    
    # Compute average validation loss for the epoch
    val_loss /= len(val_dataset)
    
    # Print training and validation loss
    print(f"Epoch {epoch+1}/{num_epochs} - Train Loss: {train_loss:.4f} - Val Loss: {val_loss:.4f}")


# for plotting predicted vs true charges in a sample of 20 molecules from the validation set (last batch)
pds = [model.forward(batch[i]).cpu().detach().numpy().reshape(-1) for i in range(20)]
truths = [batch[i].y.cpu().detach().numpy().reshape(-1) for i in range(20)]

# printing ground truths and predictions for comparison
print(' '.join(str(x) for x in np.concatenate(truths)))
print(' '.join(str(x) for x in np.concatenate(pds)))
