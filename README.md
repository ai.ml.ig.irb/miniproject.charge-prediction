# Miniproject.charge-prediction


## Getting started

This project works with PyTorch, PyTorch Geometric, Numpy and Scikit-Learning. The best way to get it to work is to create a separate virtual environment for PyTorch-based projects, e.g. using conda, and then install PyTorch and PyTorch-Geometric.

## Source data

The mini-dataset contains the set of CHARMM General Force Field (CGenFF) molecules of possible biological relevance, in a format compatible with Gromacs. Each molecule contains atoms characterized with atom types and partial charges, as well as a list of bonds between atoms and (possibly) other features relevant to the molecule's geometry.

## Technical aspects

Molecules are converted to graphs, in which atoms (vertices or nodes) are represented with a simple One-Hot encoding according to their element (6 possible values), and edges are simply 1 if two atoms are chemically bonded, or 0 otherwise. Each vertex has its true charge as a label (target for prediction).

For prediction, two generic Graph Convolutional Network layers (GCN) are applied with a ReLU activation, providing an updated embedding for each atom that takes its chemical surrounding into account. After that, this embedding is passed through a standard fully-connected (fc) layer, and through a ZeroSumOutput layer that ensures that the sum of charges is 0. (Note this requires the batch size to be equal to 1, or the ZeroSumOutputLayer to have additional functionalities.)
